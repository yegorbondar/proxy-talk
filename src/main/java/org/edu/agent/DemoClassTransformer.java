package org.edu.agent;


import javassist.*;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class DemoClassTransformer implements ClassFileTransformer {

    ClassPool classPool = new ClassPool();

    @Override
    public byte[] transform(ClassLoader loader,
                            String className,
                            Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {
        System.out.println("Transformation candidate: " + className);

        classPool.insertClassPath(new ByteArrayClassPath(className, classfileBuffer));

        try {
            CtClass ctClass = classPool.get(className.replace("/", "."));
            CtField ctField =
                new CtField(ClassPool.getDefault().get("java.lang.String"), "name", ctClass);
            ctClass.addField(ctField);
            return ctClass.toBytecode();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return classfileBuffer;
    }
}

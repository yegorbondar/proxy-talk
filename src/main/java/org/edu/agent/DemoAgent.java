package org.edu.agent;


import java.lang.instrument.Instrumentation;

public class DemoAgent {
    public static void premain(String agentArguments,
                               Instrumentation instrumentation){
        System.out.println("Demo JavaAgent Entry point. Args: " + agentArguments);
        instrumentation.addTransformer(new DemoClassTransformer());

    }
}

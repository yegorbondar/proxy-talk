package org.edu.initialization;

import java.util.Random;

public class Library {
    public boolean someLibraryMethod() throws ClassNotFoundException {
        new Child();
        //Class.forName("org/edu/initialization/Library/Child");
        //System.out.println(Child.var);
        System.out.println("------");
        new Child();
        return true;
    }
    static class Parent{
        {
            System.out.println("Parent init block before ctor");
        }

        String toString = this.toString();

        public static String var = "static variable";

        static {
            System.out.println("Parent static block");
        }

        public Parent(){
            System.out.println("Parent ctor");
        }

        {
            System.out.println("Parent init block after ctor");
        }

        @Override
        public String toString(){
            System.out.println("Parent toString call");
            return "toString()";
        }
    }
    static class Child extends Parent{

        static {
            System.out.println("Child static block");
        }

        {
            System.out.println("Child init block before ctor");
        }

        int hash = this.hashCode();

        public Child(){
            System.out.println("Child ctor");
        }

        {
            System.out.println("Child init block after ctor");
        }

        @Override
        public int hashCode(){
            System.out.println("Child hashCode call");
            return new Random().nextInt();
        }
    }
}

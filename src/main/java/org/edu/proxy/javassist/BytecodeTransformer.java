package org.edu.proxy.javassist;


import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

public class BytecodeTransformer {

    public static Class wrapAround(String className, String methodName){
        Class result = null;
        ClassPool cp = ClassPool.getDefault();
        try {
            CtClass targetClass = cp.get(className);
            CtMethod targetMethod = targetClass.getDeclaredMethod(methodName);
            targetMethod.insertBefore("{System.out.println(field);};");
            result = targetClass.toClass();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

}

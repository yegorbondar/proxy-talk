package org.edu.proxy.javassist;


import org.edu.proxy.Input;

public class Main {
    public static void main(String[] args) {
        //BytecodeTransformer transformer = new BytecodeTransformer();
        try {

            Input transformed =
                (Input) BytecodeTransformer.wrapAround(Input.class.getName(), "method")
                    .getDeclaredConstructor(String.class).newInstance("Hello world!");
            transformed.method();
        } catch (Exception ex){
            ex.printStackTrace();
        }

    }
}

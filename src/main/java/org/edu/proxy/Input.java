package org.edu.proxy;


public class Input {

    private String field;

    public Input() {
        field = "default";
    }

    public Input(String field) {
        this.field = field;
    }

    public String method() {
        return field;
    }

}

package org.edu.proxy.frameworks;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;

@Component
@Transactional
public class Main {

    @Autowired
    private SessionFactory sessionFactory;

    public static void main(String[] args) {
        Runnable runner =  (Runnable)new ClassPathXmlApplicationContext("applicationContext.xml").getBean(Runnable.class);
        System.out.println(runner.getClass().getName());
        runner.run();
 //       new ClassPathXmlApplicationContext("applicationContext.xml").getBean(Main.class).run();
    }
    private void run(){
        Session session = sessionFactory.getCurrentSession();
        final Book b1 = new Book();
        b1.setTitle("CoolStory");
        Author a1 = new Author();
        a1.setName("CoolMan");
        a1.setBooks(new HashSet<Book>(){{
            add(b1);
        }});
        session.saveOrUpdate(a1);

        List<Author> authors = (List<Author>)session.createQuery("from Author").list();
        System.out.println(authors);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}

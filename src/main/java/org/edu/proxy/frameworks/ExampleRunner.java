package org.edu.proxy.frameworks;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;


@Service
//@Transactional
public class ExampleRunner implements Runnable{

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public void run() {
        System.out.println(this.getClass().getName());
        Session session = sessionFactory.getCurrentSession();
        final Book b1 = new Book();
        b1.setTitle("CoolStory");
        b1.setDescription("CoolBook");
        final Book b2 = new Book();
        b2.setTitle("CoolStory2");
        b2.setDescription("CoolBook2");
        Author a1 = new Author();
        a1.setName("CoolMan");
        a1.setBooks(new HashSet<Book>(){{
            add(b1);
            add(b2);
        }});
        session.saveOrUpdate(a1);
        session.flush();
        session.clear();

        List<Author> authors = (List<Author>)session.createQuery("from Author").list();
        session.close();
        System.out.println(authors.get(0).getBooks().iterator().next().getClass());
        System.out.println(authors);
    }
}

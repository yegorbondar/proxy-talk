import org.edu.initialization.Library;
import org.junit.Test;
import static org.junit.Assert.*;


public class LibraryTest {
    @Test public void testSomeLibraryMethod() throws ClassNotFoundException {
        Library classUnderTest = new Library();
        assertTrue("someLibraryMethod should return 'true'", classUnderTest.someLibraryMethod());
    }
}

package org.edu.proxy.javassist;


import org.edu.proxy.Input;
import org.junit.Before;
import org.junit.Test;

public class BytecodeTransformerTest {

    private BytecodeTransformer transformer;

    @Before
    public void configureTransformer() {
        transformer = new BytecodeTransformer();
    }

    @Test
    public void shouldWrapMethodBodyWithSOUT() throws Exception {
        Input transformed =
            (Input) transformer.wrapAround(Input.class.getCanonicalName(), "method")
                .getDeclaredConstructor(String.class).newInstance("Hello world!");
        transformed.method();
    }
}
